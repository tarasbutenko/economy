﻿using Economy_programm.Classes;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Economy_programm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int _rowsCount = 6;
        public static int _columnsCount = 15;
        private CheckBox[][] array;
        private TextBlock[] arrayTB;
        public MainWindow()
        {
            InitializeComponent();
            setupForUI();
            arrayTB = new TextBlock[10];
            const int rowIndentation = 1;
            const int columnWithInformation = 17;
            for (int i=0; i<arrayTB.Count();i++)
            {
                arrayTB[i] = new TextBlock();
                Grid.SetRow(arrayTB[i], i + rowIndentation);
                Grid.SetColumn(arrayTB[i], columnWithInformation);
                MyGrid.Children.Add(arrayTB[i]);
            }
            //var calculator = new Calculator();
            //                                 //     1      2     3     4       5       6   7       8       9    10     11      12      13  14      15
            //calculator.boolArray[0] = new bool[] { true , true , true ,true , false, true, false, false, false, false, false, true , true, true , false};
            //calculator.boolArray[1] = new bool[] { false, false, false, true , true , true, true , false, false, false, true, true , false, true , true};
            //calculator.boolArray[2] = new bool[] { false, true , true, false , false , false, false , true, true, true, false, true , true, false , true};
            //calculator.boolArray[3] = new bool[] { false, false, true, false , false , true, true , true, true, false, true, false , true, false , true};
            //calculator.boolArray[4] = new bool[] { false, true,  true, true , true , false, false , true, false, true, false, true , false, true , false};
            //calculator.boolArray[5] = new bool[] { true, false, true, false, true, false, true , false, true, false, true, false , true, false , true};

            //var result = calculator.FirstStep();

            //calculator.dictionary.Add("AB",result[0]);
            //calculator.dictionary.Add("AC",result[1]);
            //calculator.dictionary.Add("AD",result[2]);
            //calculator.dictionary.Add("AK",result[3]);
            //calculator.dictionary.Add("AL",result[4]);

            //calculator.dictionary.Add("BC", result[5]);
            //calculator.dictionary.Add("BD", result[6]);
            //calculator.dictionary.Add("BK", result[7]);
            //calculator.dictionary.Add("BL", result[8]);

            //calculator.dictionary.Add("CD", result[9]);
            //calculator.dictionary.Add("CK", result[10]);
            //calculator.dictionary.Add("CL", result[11]);

            //calculator.dictionary.Add("DK", result[12]);
            //calculator.dictionary.Add("DL", result[13]);

            //calculator.dictionary.Add("KL", result[14]);

            //var resultD = calculator.SecondStep();

            //ShowResults(resultD);
            //int i = 0;          
        }

        private void setupForUI()
        {
            const int rowIndentation=2;
            const int rowWithInformation =1;
            const int columnIndentation =2;
            array = new CheckBox[_rowsCount][];

            for (int i = 0; i < _rowsCount; i++)
            {
                array[i] = new CheckBox[_columnsCount];
                for (int j = 0; j < _columnsCount; j++)
                {
                    array[i][j]=new CheckBox();
                    var b = array[i][j];
                    b.Visibility = Visibility.Visible;

                    Grid.SetRow(b, i+rowIndentation);
                    Grid.SetColumn(b, j+columnIndentation);
                    MyGrid.Children.Add(b);

                    var r = new Rectangle();
                    SolidColorBrush blackBrush = new SolidColorBrush(Colors.Black);
                    r.StrokeThickness = 0.2;
                    r.Stroke = blackBrush;
                    Grid.SetRow(r, i + rowIndentation);
                    Grid.SetColumn(r, j + columnIndentation);
                    MyGrid.Children.Add(r);
                }
            }
            for (int i = 0; i < _columnsCount; i++)
            {
                var tb = new TextBlock();
                tb.Text = "д" + (i + 1).ToString();
                Grid.SetRow(tb, rowWithInformation);
                Grid.SetColumn(tb,i+columnIndentation );
                MyGrid.Children.Add(tb);

                var r = new Rectangle();
                SolidColorBrush blackBrush = new SolidColorBrush(Colors.Black);
                r.StrokeThickness = 0.2;
                r.Stroke = blackBrush;

                Grid.SetRow(r, rowWithInformation);
                Grid.SetColumn(r, i + columnIndentation);
                MyGrid.Children.Add(r);

            }
        }
        private void ShowResults(Dictionary<string , double> d)
        {
            const int rowIndentation = 1;
            const int columnWithInformation = 17;
            
            var min = d.First();
            foreach (var kvp in d)
            {
                if (kvp.Value < min.Value)
                    min = kvp;
            }

            var i = 0;
        

            foreach (var kvp in d)
            {
                var tb = arrayTB[i++];
                tb.Text= kvp.Key+"  "+ kvp.Value.ToString();
                if (kvp.Value == min.Value)
                {
                    tb.Text += "-> min";
                }
           }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var calculator = new Calculator();
            for (int i = 0; i < _rowsCount; i++)
            {
                for (int j = 0; j < _columnsCount; j++)
                {
                    calculator.boolArray[i][j] = array[i][j].IsChecked.Value;
                }
            }
            var result = calculator.FirstStep();

            calculator.dictionary.Add("AB", result[0]);
            calculator.dictionary.Add("AC", result[1]);
            calculator.dictionary.Add("AD", result[2]);
            calculator.dictionary.Add("AK", result[3]);
            calculator.dictionary.Add("AL", result[4]);

            calculator.dictionary.Add("BC", result[5]);
            calculator.dictionary.Add("BD", result[6]);
            calculator.dictionary.Add("BK", result[7]);
            calculator.dictionary.Add("BL", result[8]);

            calculator.dictionary.Add("CD", result[9]);
            calculator.dictionary.Add("CK", result[10]);
            calculator.dictionary.Add("CL", result[11]);

            calculator.dictionary.Add("DK", result[12]);
            calculator.dictionary.Add("DL", result[13]);

            calculator.dictionary.Add("KL", result[14]);

            var resultD = calculator.SecondStep();

            ShowResults(resultD);
        }
    } 
}
