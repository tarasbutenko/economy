﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Economy_programm.Classes
{
    public class Calculator
    {
        public static int _rowsCount = 6;
        public static int _columnsCount = 15;
        public static int doubleArraySize = 15;
        public int rowsCount;
        public int columnsCount;
        static double numerator = 0;
        static double denominator = 0;
        public Dictionary<string, double> dictionary;
        public bool[][] boolArray;
        public Calculator()
        {
            boolArray = new bool[_rowsCount][];

            for (int i = 0; i < _rowsCount; i++)
            {
                boolArray[i] = new bool[_columnsCount];
            }
            dictionary = new Dictionary<string, double>();
        }

        public Calculator(bool[][] array)
        {
            boolArray = array;
            rowsCount = array.GetLength(0);
            columnsCount = array[0].GetLength(0);
        }
        public double[] FirstStep()
        {
            var doubleArray = new double[doubleArraySize];
            bool first, second;
            int counter = 0;
            for (int i = 0; i < _rowsCount - 1; i++)
            {
                for (int i2 = i + 1; i2 < _rowsCount; i2++)
                {
                    int x1=0, x2=0;
                    for (int j = 0; j < _columnsCount; j++)
                    {
                        first = boolArray[i][j];
                        second = boolArray[i2][j];
                        if (first && second)
                            numerator++;
                        if (first )
                            x1++;
                        if (second)
                            x2++;
                    }
                    denominator = Math.Sqrt(x1 * x2);
                    doubleArray[counter] = 1 - (numerator / denominator) ;
                    counter++;
                    numerator = 0;
                    denominator = 0;
                }
            }
            return doubleArray;
        }

        public Dictionary<string, double> SecondStep()
        {
            var d = new Dictionary<string, double>();
            d.Add("ABCDKL", I(dictionary, "ABCDKL".ToArray()));
            d.Add("ABDCKL", I(dictionary, "ABDCKL".ToArray()));
            d.Add("ABKDCL", I(dictionary, "ABKCDL".ToArray()));
            d.Add("ABLCDK", I(dictionary, "ABLCDK".ToArray()));
            d.Add("ACDBKL", I(dictionary, "ACDBKL".ToArray()));
            d.Add("ACKBDL", I(dictionary, "ACKBDL".ToArray()));
            d.Add("ACLBDK", I(dictionary, "ALCDKB".ToArray()));
            d.Add("ADKBCL", I(dictionary, "ADKBCL".ToArray()));
            d.Add("ADLBCK", I(dictionary, "ADLBCK".ToArray()));
            d.Add("AKLBCD", I(dictionary, "AKLBCD".ToArray()));

            return d;
        }

        private double I(Dictionary<string, double> dict, char[] a)
        {
            string firstWord = new string(a, 0, 3);
            string secondWord = new string(a, 3, 3);
            var a1 = firstWord.ToArray();
            var a2 = secondWord.ToArray();
            Array.Sort(a1);
            Array.Sort(a2);
            double value = 0;
            value+=dict[new string(a1, 0, 2)];
            value += dict[new string(a1, 1, 2)];
            value += dict[a1[0].ToString() + a1[2].ToString()];

            value += dict[new string(a2, 0, 2)];
            value += dict[new string(a2, 1, 2)];
            value += dict[a1[0].ToString() + a2[2].ToString()];

            return value;
        }
    }
}
